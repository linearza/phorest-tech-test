import {
  module,
  test
} from 'qunit';
import {
  setupRenderingTest
} from 'ember-qunit';
import {
  render
} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Component | p-header', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs `{{p-header}}`);

    assert.equal(this.element.querySelectorAll('li').length, 2, 'two menu items are rendered');
    assert.equal(this.element.querySelectorAll('li')[0].textContent, 'Home', 'the first is Home');
    assert.equal(this.element.querySelectorAll('li')[1].textContent, 'Clients', 'the second is Clients');

  });
});

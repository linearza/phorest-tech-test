import {
  module,
  test
} from 'qunit';
import {
  visit,
  currentURL,
  fillIn,
  click
} from '@ember/test-helpers';
import {
  setupApplicationTest
} from 'ember-qunit';

module('Acceptance | clients listing', function(hooks) {
  setupApplicationTest(hooks);

  test('visiting /clients', async function(assert) {
    await visit('/clients');
    assert.equal(currentURL(), '/clients');
  });

  test('check intial render', async function(assert) {

    await visit('/clients');
    assert.equal(currentURL(), '/clients');

    assert.equal(this.element.querySelectorAll('.row.client').length, 20, 'twenty client result row items are rendered');
    // Just for demo purposes, in reality we wouldn't want to check a specific amount of records, unless we use fixture data
    assert.equal(this.element.querySelector('.t-total-pages').textContent, "49", 'there are 49 pages in total');
    assert.equal(this.element.querySelector('.t-current-page').textContent, "1", 'currently on page 1');
  });

  test('should filter by inputs', async function(assert) {

    await visit('/clients');
    assert.equal(currentURL(), '/clients');

    await fillIn('input.t-first-name', 'badfilter');
    assert.equal(this.element.querySelectorAll('.row.client').length, 0, 'no results are returned for "badfilter"');

    await fillIn('input.t-first-name', 'ba');
    assert.equal(this.element.querySelectorAll('.row.client').length, 7, '7 results are returned for "ba"');

    await fillIn('input.t-first-name', 'babs');
    assert.equal(this.element.querySelectorAll('.row.client').length, 1, '1 results are returned for "babs"');

    await click('.t-exact-match');
    await fillIn('input.t-first-name', 'bab');
    assert.equal(this.element.querySelectorAll('.row.client').length, 0, '0 results are returned for "bab" with exact match');

  });
});

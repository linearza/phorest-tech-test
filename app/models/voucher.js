import DS from 'ember-data';

export default DS.Model.extend({

  voucherId: DS.attr(),
  clientId: DS.attr(),
  creatingBranchId: DS.attr(),
  expiryDate: DS.attr(),
  issueDate: DS.attr(),
  links: DS.attr(),
  originalBalance: DS.attr('number'),
  remainingBalance: DS.attr('number'),
  serialNumber: DS.attr(),

  // client: DS.belongsTo('client')

});

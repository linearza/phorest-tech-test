import DS from 'ember-data';

export default DS.Model.extend({

  address: DS.attr(),
  archived: DS.attr(),
  banned: DS.attr(),
  clientId: DS.attr(),
  clientSince: DS.attr(),
  creatingBranchId: DS.attr(),
  creditAccount: DS.attr(),
  email: DS.attr(),
  emailMarketingConsent: DS.attr(),
  emailReminderConsent: DS.attr(),
  firstName: DS.attr(),
  gender: DS.attr(),
  lastName: DS.attr(),
  mobile: DS.attr(),
  preferredStaffId: DS.attr(),
  smsMarketingConsent: DS.attr(),
  smsReminderConsent: DS.attr(),
  version: DS.attr(),

  // vouchers: DS.hasMany('voucher')

});

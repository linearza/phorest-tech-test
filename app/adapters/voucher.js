import ApplicationAdapter from 'phorest-tech-test/adapters/application';

export default ApplicationAdapter.extend({

  pathForType: function() {
    return 'voucher';
  }

});

import ApplicationAdapter from 'phorest-tech-test/adapters/application';

import {
  inject as service
} from '@ember/service';

export default ApplicationAdapter.extend({

  session: service(),

  pathForType: function() {
    return 'client';
  },

  buildURL(modelName, id, snapshot, requestType, query) {

    if (!this.session.exactMatch && query) {
      if (query.firstName) {
        query.firstName = '~' + query.firstName + '%';
      }
      if (query.lastName) {
        query.lastName = '~' + query.lastName + '%';
      }
    }

    return this._super(...arguments);
  }

});

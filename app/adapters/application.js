import DS from 'ember-data';
import config from 'phorest-tech-test/config/environment';

import {
  inject as service
} from '@ember/service';
import {
  computed
} from 'ember-decorators/object';

export default DS.RESTAdapter.extend({

  session: service(),

  host: 'http://api-gateway-dev.phorest.com',

  @computed()
  namespace() {
    return 'third-party-api-server/api/business/' + this.get('session.businessId');
  },

  @computed()
  headers() {
    return {
      Authorization: 'Basic ' + btoa(config.APP.apiUsername + ":" + config.APP.apiPassword)
    };
  }

});

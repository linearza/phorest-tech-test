import Route from '@ember/routing/route';
import {
  inject as service
} from '@ember/service';
import moment from 'moment';

export default Route.extend({

  session: service(),
  flashMessages: service(),

  setupController() {
    this.createClient();
  },

  createClient() {
    let client = this.store.createRecord('client', {
      creatingBranchId: this.session.get('branchId')
    });
    this.controller.set('client', client);
  },

  resetController(controller, isExiting) {
    if (isExiting) {
      controller.set('saving', false);
    }
  },

  actions: {
    saveClient(client) {
      if (this.controller.get('saving')) {
        return;
      }

      this.controller.set('saving', true);

      client.set('clientSince', moment().utc().format());

      client.save().then((client) => {
        this.controller.set('saving', false);
        this.flashMessages.success(client.firstName + ' ' + client.firstName + ' created!');
        this.createClient();
      }).catch(() => {
        this.flashMessages.danger('Error! Something went wrong');
        this.controller.set('saving', false);
      })
    },

    goBack() {
      window.history.back();
    }
  }
});

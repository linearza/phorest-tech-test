import Service from '@ember/service';

export default Service.extend({

  // account credentials
  businessId: 'eTC3QY5W3p_HmGHezKfxJw',
  branchId: 'SE-J0emUgQnya14mOGdQSw',

  // search preference
  exactMatch: false

});

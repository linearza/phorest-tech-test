import Controller from '@ember/controller';
import {
  computed
} from 'ember-decorators/object';
import {
  task,
  timeout
} from 'ember-concurrency';
import {
  inject as service
} from '@ember/service';

import QueryParams from 'ember-parachute';
export const clientsQueryParams = new QueryParams({
  page: {
    defaultValue: 0,
    refresh: true,
    replace: true
  },
  firstName: {
    defaultValue: undefined,
    refresh: true,
    replace: true
  },
  lastName: {
    defaultValue: undefined,
    refresh: true,
    replace: true
  },
  email: {
    defaultValue: undefined,
    refresh: true,
    replace: true
  },
  phone: {
    defaultValue: undefined,
    refresh: true,
    replace: true
  }
});

export default Controller.extend(clientsQueryParams.Mixin, {

  session: service(),

  page: 0,
  size: 20,

  phone: undefined,
  email: undefined,
  firstName: undefined,
  lastName: undefined,

  didChangePage: false,

  setup({
    queryParams
  }) {
    this.fetchData(queryParams);
  },

  queryParamsDidChange({
    shouldRefresh,
    queryParams
  }) {
    if (shouldRefresh) {
      if (!this.get('didChangePage')) {
        this.set('page', 0);
        queryParams.page = 0;
      }

      /*
        Unfortunately the server cant handle empty strings,
        and clearing an input manually leaves that as the value,
        so we need to manually override it here in such cases
      */
      if (queryParams.firstName === '') {
        this.set('firstName', undefined);
        queryParams.firstName = undefined;
      }

      if (queryParams.lastName === '') {
        this.set('lastName', undefined);
        queryParams.lastName = undefined;
      }

      if (queryParams.email === '') {
        this.set('email', undefined);
        queryParams.email = undefined;
      }

      if (queryParams.phone === '') {
        this.set('phone', undefined);
        queryParams.phone = undefined;
      }

      this.fetchData(queryParams);
    }
  },

  fetchData(queryParams) {
    this.get('fetchDataTask').perform(queryParams);
  },

  fetchDataTask: task(function*(query) {
    yield timeout(1000);

    let clients = yield this.get('store').query('client', query);

    // reset to top of page
    window.scrollTo(0, 0);

    return this.setProperties({
      clients: clients,
      pageTotalElements: clients.meta.totalElements,
      pageTotalPages: clients.meta.totalPages,
      pageSize: clients.meta.size,
      didChangePage: false,
    })
  }).restartable(),

  @computed('page')
  hasPrevPage(page) {
    return page > 0
  },

  @computed('displayPage', 'totalPages')
  hasNextPage(displayPage, totalPages) {
    return displayPage < totalPages;
  },

  @computed('page')
  displayPage(page) {
    return page + 1;
  },

  @computed('pageTotalElements', 'size')
  totalPages(pageTotalElements, size) {
    if (pageTotalElements && size) {
      return Math.max(Math.round(pageTotalElements / size), 1);
    }
  },

  actions: {
    nextPage() {
      if (this.get('displayPage') === this.get('totalPages')) {
        return;
      }

      this.set('didChangePage', true);
      this.incrementProperty('page');
    },

    prevPage() {
      if (this.get('page') === 0) {
        return;
      }
      this.set('didChangePage', true);
      this.decrementProperty('page');
    },

    resetFilters() {
      this.resetQueryParams();
    }

  }
});

import Route from '@ember/routing/route';
import {
  inject as service
} from '@ember/service';

export default Route.extend({

  session: service(),

  resetController(controller, isExiting) {
    if (isExiting) {
      controller.set('page', 0);
    }
  }
});

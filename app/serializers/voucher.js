import DS from 'ember-data';
import {
  merge
} from '@ember/polyfills';

export default DS.RESTSerializer.extend({

  primaryKey: 'voucherId',

  serializeIntoHash(data, type, record, options) {
    merge(data, this.serialize(record, options));
  },

  normalizeResponse(store, primaryModelClass, payload, id, requestType) {

    if (requestType === "createRecord") {
      let voucher = payload ? payload : {}
      payload = {
        voucher: voucher
      }
    }

    if (requestType === 'query') {
      let vouchers = payload._embedded ? payload._embedded.vouchers : []

      payload = {
        vouchers: vouchers,
        meta: payload.page
      };
    }

    return this._super(store, primaryModelClass, payload, id, requestType);
  }



});

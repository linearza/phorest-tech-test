import DS from 'ember-data';
import {
  merge
} from '@ember/polyfills';

export default DS.RESTSerializer.extend({

  primaryKey: 'clientId',

  serializeIntoHash(data, type, record, options) {
    merge(data, this.serialize(record, options));
  },

  normalizeResponse(store, primaryModelClass, payload, id, requestType) {

    if (requestType === "createRecord") {
      let client = payload ? payload : {}
      payload = {
        client: client
      }
    }

    if (requestType === 'findRecord') {
      let client = payload ? payload : {}

      payload = {
        client: client
      };
      return this._super(store, primaryModelClass, payload, id, requestType);
    }

    if (requestType === "query") {
      let clients = payload._embedded ? payload._embedded.clients : []

      payload = {
        clients: clients,
        meta: payload.page
      };
    }

    return this._super(store, primaryModelClass, payload, id, requestType);
  }
});

import Route from '@ember/routing/route';
import {
  inject as service
} from '@ember/service';
import moment from 'moment';

export default Route.extend({

  session: service(),
  flashMessages: service(),

  model() {
    return this.modelFor('client.vouchers');
  },

  setupController(controller, model) {
    this._super(...arguments);

    controller.set('client', model);
    this.createVoucher();
  },

  createVoucher() {
    this.controller.set('voucher', this.store.createRecord('voucher', {
      clientId: this.controller.client.get('id'),
      creatingBranchId: this.session.get('branchId')
    }));
  },

  resetController(controller, isExiting) {
    if (isExiting) {
      controller.set('saving', false);
    }
  },

  actions: {
    saveVoucher(voucher) {
      if (this.controller.get('saving')) {
        return;
      }

      this.controller.set('saving', true);

      /*
        Just hardcoding the expiry date one day into the future for now
      */
      voucher.setProperties({
        issueDate: moment().utc().format(),
        expiryDate: moment().add(1, 'days').utc().format()
      })

      voucher.save().then(() => {
        this.controller.set('saving', false);
        this.flashMessages.success('Voucher saved!');
        this.send('reloadVouchers');
        this.createVoucher();
      }).catch(() => {
        this.flashMessages.danger('Error! Something went wrong');
        this.controller.set('saving', false);
      })
    },

    goBack() {
      window.history.back();
    }
  }
});

import Route from '@ember/routing/route';

export default Route.extend({
  model() {
    return this.modelFor('client');
  },

  setupController(controller, model) {
    this._super(...arguments);

    let allVouchers = this.store.query('voucher', {
      clientId: model.id
    });

    controller.set('vouchers', allVouchers);
    controller.set('client', model);
  },

  actions: {
    reloadVouchers() {
      let allVouchers = this.store.query('voucher', {
        clientId: this.controller.client.id
      });

      this.controller.set('vouchers', allVouchers);
    }
  }
});

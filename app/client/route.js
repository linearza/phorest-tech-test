import Route from '@ember/routing/route';

export default Route.extend({
  model(params) {
    if (params.client_id) {
      return this.store.findRecord('client', params.client_id);
    }
  }
});

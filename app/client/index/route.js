import Route from '@ember/routing/route';

export default Route.extend({
  model(params) {
    return this.modelFor('client');
    // return this.store.findRecord('client', params.client_id);
  },

  setupController(controller, model) {
    this._super(...arguments);

    controller.set('client', model);
  }
});

import {
  helper as buildHelper
} from '@ember/component/helper';

export function capitalize([toCapitalise] /*, hash*/ ) {
  if (toCapitalise) {
    return toCapitalise.charAt(0).toUpperCase() + toCapitalise.slice(1);
  }
}

export default buildHelper(capitalize);

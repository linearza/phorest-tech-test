import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('clients');

  this.route('client', {
    path: 'client/:client_id'
  }, function() {
    this.route('index', {
      path: ''
    });
    this.route('vouchers', {
      path: '/vouchers'
    }, function() {
      this.route('new');
    });
  });

  this.route('new-client', {
    path: 'client/new'
  });

});

export default Router;
